@extends('master')

@section('judul')
Halaman Welcome
@endsection

@section('content')
    <h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}}!</h1>
    <p>Selamat bergabung diWebsite kami. Selamat belajar!</p>
@endsection