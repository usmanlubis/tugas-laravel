<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }

    public function kirim(Request $request){
        $namaDepan = $request['firstName'];
        $namaBelakang = $request['lastName'];
        return view('welcome', compact('namaDepan', 'namaBelakang'));
    }
}
